<?php

namespace App\Controller;

use App\Service\ContactFormService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ContactFormController extends AbstractController
{

    public function __construct(private readonly ContactFormService $contactFormService)
    {}

    #[Route('/contact-form', name: 'get_contact_form', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('contact_form/index.html.twig', [
            'controller_name' => 'ContactFormController',
        ]);
    }

    #[Route('/contact-form', name: 'post_contact_form', methods: ['POST'])]
    public function save(Request $request): Response
    {
        $data = $request->getContent();
        $result = $this->contactFormService->saveForm($data);

        return new JsonResponse(
            $result,
            $result['id'] ? 200 : 400
        );
    }
}
