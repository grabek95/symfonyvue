<?php

namespace App\Service;

use App\Entity\ContactForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContactFormService
{
    const SUCCESS_MESSAGE = "Formularz zapisano poprawnie!";
    const ERROR_MESSAGE = "Formularz zapisano poprawnie!";
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ValidatorInterface $validator,
        private readonly SerializerInterface $serializer
    ) {}

    public function saveForm(string $data): array
    {
        $contactForm = $this->serializer->deserialize($data, ContactForm::class, 'json', ['groups' => 'write']);

        $errors = $this->validator->validate($contactForm);
        if (!$errors->count()) {
            $this->entityManager->persist($contactForm);
            $this->entityManager->flush();
        }

        $id = $contactForm->getId();
        $message = $id
            ? self::SUCCESS_MESSAGE
            : self::ERROR_MESSAGE;

        return [
            'id' => $id,
            'contact_form' => $this->serializer->serialize($contactForm, 'json', ['groups' => 'read']),
            'message' => $message
        ];

    }
}