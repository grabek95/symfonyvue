## Instrukcja
* Projekt można odpalić za pomocą skryptu run.sh.
  * w systemie Linux bash run.sh
* Formularz znajduje sie pod adresem:
  * http://localhost:8080/contact-form
* Dostęp do bazy Mysql:
  * 127.0.0.1
  * port 5306
  * Użytkownik: user
  * Hasło: secret
  * Hasło użytkownika root: secreroot
  * Baza danych: soulab-db