docker-compose --env-file .docker/.env -f .docker/docker-compose.yml up -d
docker exec -it soulab-php composer install
docker exec -it soulab-php npm install
docker exec -it soulab-php npm run build
docker exec -it soulab-php php bin/console doctrine:migrations:migrate --no-interaction
